package com.lmkr.osdu.elastic;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;


public class ElasticClient {
	@Value("${elastic.cluster.host}")
	private String esHost;

	@Value("${elastic.cluster.port}")
	private int esPort;

	@Value("${elastic.cluster.index}")
	private String esIndex;

	public RestHighLevelClient getElasticClient(){
		return new RestHighLevelClient(RestClient.builder(
				new HttpHost("localhost",9200, "http")).setRequestConfigCallback(
						new RestClientBuilder.RequestConfigCallback() {
							@Override
							public RequestConfig.Builder customizeRequestConfig(
									RequestConfig.Builder requestConfigBuilder) {
								return requestConfigBuilder
										.setConnectTimeout(500000)
										.setConnectionRequestTimeout(500000)
										.setSocketTimeout(6000000);
							}
						})
				);
	}


	/////////////By irfan******************
	public int elasticIndexOsdu(ResultSet rs) throws InterruptedException {

		System.out.println("\n[Performing Insert]");
		RestHighLevelClient client = null;
		RestHighLevelClient clientDr = null;
		ResultSetMetaData metaData = null;
		BulkRequest bulkRequest = new BulkRequest();

		Double lat=0.0;
		Double lon=0.0;
		int r = 0;
		int failed = 0;
		try {
			//Preparing Elastic Client
			client = getElasticClient();
			metaData =  rs.getMetaData();
			int vv = 0;

			while (rs.next()) {
				JSONObject rowObj = new JSONObject();
				Boolean containsNullGeom = false;
				int count = 0;
				// rowObj.put("_id", rs.getInt("id")+"_"+rs.getInt("fkey"));
				for (int i = 1; i <= metaData.getColumnCount(); i++) {
					count++;


					rowObj.put(metaData.getColumnName(i), rs.getObject(metaData.getColumnName(i)));


				}

				Map<String, Object> OsduJsonObject=getOsduMapping(rs);
				bulkRequest.add(new IndexRequest("osdu").source(OsduJsonObject));
				//bulkRequest.add(new IndexRequest().source(OsduJsonObject));


			}
			if (bulkRequest.numberOfActions() < 1) {
				r = 0;
				System.out.println("[No Item in bulk action]");
			} else {
				//bulkRequest.pipeline("pipelineId");
				BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
				r = bulkResponse.getItems().length;
				System.out.println("[Insert] Insert requests : " + r);



				if (bulkResponse.hasFailures()) {
					// process failures by iterating through each bulk response item
					for (BulkItemResponse bulkItemResponse : bulkResponse) {
						failed++;

					}
					throw new SQLException(failed+++bulkResponse.buildFailureMessage() + " - Validate Mapping of Index===" + bulkResponse.getItems());

				}


			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				rs.close();
				client.close();

				System.out.println("****************************\n\n");


			} catch (Exception  ex) {
				ex.printStackTrace();
			}
		}

		System.out.println("r===: " + r+"   ***failed======"+failed);
		return r-failed;


	}

	private Map<String, Object> getOsduMapping(ResultSet resultSet) throws SQLException {
		Map<String, Object> osduJsonObject=new HashMap<>();

		osduJsonObject.put("email", resultSet.getString("email") == null ? "": resultSet.getString("email"));
		osduJsonObject.put("password", resultSet.getString("password")== null ? "": resultSet.getString("password"));
		osduJsonObject.put("role", resultSet.getString("role")== null ? "": resultSet.getString("role"));
		return osduJsonObject;
	}

}
