package com.lmkr.osdu.service.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import com.lmkr.osdu.service.TokenService;

@Service
public class TokenServiceImpl implements TokenService {
public String getToken() {
	String token="";
	token=UUID.randomUUID().toString();
	return token;
}
}
