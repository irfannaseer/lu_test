package com.lmkr.osdu.service.impl;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lmkr.osdu.dao.DataDao;
import com.lmkr.osdu.service.DataService;

@Service
public class DataServiceImpl implements DataService {
	@Autowired
	DataDao dataDao;

	@Override
	public long getTotalRecords(String sql) {
		// TODO Auto-generated method stub
		return dataDao.getTotalRecords(sql);
	}

	@Override
	public JSONArray getRecords(long totalRecords, String sql) {
		// TODO Auto-generated method stub
		//System.out.println("DataServiceImpl========="+dataDao.getRecords(totalRecords, sql));
		return dataDao.getRecords(totalRecords, sql);
	}

	@Override
	public JSONArray getRecordsFromPg(long totalRecords, String sql) {
		// TODO Auto-generated method stub
		return dataDao.getRecordsFromPostgres(totalRecords, sql);
	}

	@Override
	public JSONArray getRecordsFromH2(long totalRecords, String sql) {
		// TODO Auto-generated method stub
		return dataDao.getRecordsFromH2(totalRecords, sql);
	}

	@Override
	public JSONArray getRecordsFromMultipleDb(String query, String tableName, String dataSource) {
		// TODO Auto-generated method stub
		String sql="Select "+query+" from "+tableName;
		return dataDao.getRecordsFromMultipleDb(sql,dataSource);
	}

	@Override
	public JSONArray getRecordsFromMultipleDb(String query, String whereId, String tableName, String dataSource) {
		String sql="Select "+query+" from "+tableName+" where id="+whereId;
		return dataDao.getRecordsFromMultipleDb(sql,dataSource);
	}
	
	

}
