package com.lmkr.osdu.service;

import org.json.JSONArray;
import org.json.JSONObject;

public interface DataService {
	public long getTotalRecords(String sql);
	public JSONArray getRecords(long totalRecords,String sql);
	public JSONArray getRecordsFromPg(long totalRecords,String sql);
	public JSONArray getRecordsFromH2(long totalRecords,String sql);
	public JSONArray getRecordsFromMultipleDb(String query,String tableName,String dataSource);
	public JSONArray getRecordsFromMultipleDb(String query,String whereId,String tableName,String dataSource);
}
