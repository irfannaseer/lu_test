package com.lmkr.osdu.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.zaxxer.hikari.HikariDataSource;


@Configuration
public class DataSourceConfig{
	 
//	@Bean(name = "postgres")
//    @Qualifier("primaryDataSource")
//    @ConfigurationProperties(prefix="spring.datasource")
//    public DataSource primaryDataSource(){
//        return DataSourceBuilder.create().build();
//    }
//
//
//    @Bean(name = "ecommerce")
//    @Qualifier("secondaryDataSource")
//    @ConfigurationProperties(prefix="spring.second-db")
//    public DataSource secondaryDataSource(){
//        return DataSourceBuilder.create().build();
//    }
	

//	@Bean(name = "postgresJdbcTemplate")
//	public JdbcTemplate postgresJdbcTemplate(@Qualifier("postgresDb") 
//                                              DataSource dsPostgres) {
//		return new JdbcTemplate(dsPostgres);
//	}
//	
	
	@Bean(name = "ecommerce")
	 @ConfigurationProperties(prefix = "spring.datasource")
	 public DataSource dataSource1(DataSourceProperties properties) {
	  return DataSourceBuilder.create(properties.getClassLoader())
				.type(HikariDataSource.class)
				.driverClassName(properties.determineDriverClassName())
				.url(properties.determineUrl())
				.username(properties.determineUsername())
				.password(properties.determinePassword())
				.build();
	 }

	 @Bean(name = "jdbcTemplate1")
	 public JdbcTemplate jdbcTemplate1(@Qualifier("ecommerce") DataSource ds) {
	  return new JdbcTemplate(ds);
	 }
	 
	
	 @Bean(name = "postgres")
	 @ConfigurationProperties(prefix = "spring.second-db")
	 public DataSource dataSource2(DataSourceProperties properties) {
	  return  DataSourceBuilder.create(properties.getClassLoader())
				.type(HikariDataSource.class)
				.driverClassName(properties.determineDriverClassName())
				.url("jdbc:postgresql://localhost:5432/postgres?user=postgres&password=postgres")
				.username(properties.determineUsername())
				.password(properties.determinePassword())
				.build();
	 }

	 @Bean(name = "jdbcTemplate2")
	 public JdbcTemplate jdbcTemplate2(@Qualifier("postgres") DataSource ds) {
	  return new JdbcTemplate(ds);
	 }
	 
	 @Bean(name = "mysqlDatasource")
	  @ConfigurationProperties(prefix = "mysql.datasource")
	  public DataSource mysqlDatasource() {
	    return DataSourceBuilder.create().build();
	  }
	 
	 @Bean(name = "mysqlJdbcTemplate")
	  public NamedParameterJdbcTemplate mysqlJdbcTemplate(
	      @Qualifier("mysqlDatasource") DataSource dataSource) {
	    return new NamedParameterJdbcTemplate(dataSource);
	  }
	 
	 @Bean(name = "pgDatasource")
	  @ConfigurationProperties(prefix = "spring.datasource")
	  public DataSource pgDatasource() {
	    return DataSourceBuilder.create()
	    		.url("jdbc:postgresql://localhost:5432/ecommerce?user=postgres&password=postgres")
	    		.build();
	  }
	 
	 @Bean(name = "pgJdbcTemplate")
	  public NamedParameterJdbcTemplate pgJdbcTemplate(
	      @Qualifier("pgDatasource") DataSource dataSource) {
	    return new NamedParameterJdbcTemplate(dataSource);
	  }
}