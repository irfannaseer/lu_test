package com.lmkr.osdu.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.FileWriter;

import com.lmkr.osdu.service.DataService;
import com.lmkr.osdu.service.TokenService;

@RequestMapping("/token")
@RestController  
//Osdu controller
// add one more
public class TokenController {
	@Autowired
	TokenService tokenService;

	@Autowired
	DataService dataService;

	@GetMapping("/gettoken")  
	public String getToken(){  
		return tokenService.getToken();  
	}  
	@GetMapping("/getcount")  
	public long getCount(){  
		String sql="Select count(*) from products";
		return dataService.getTotalRecords(sql);  
	} 

	@GetMapping("/getdata")  
	public String getData(){  
		//String sql="Select id,name,token from products limit ? offset ?";
		String sql="Select * from products";
		return dataService.getRecords(3,sql).toString();  
	} 

	@GetMapping("/getdatafromfile")  
	public String getDataFromFile(){  
		JSONArray jsonList =null;
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("islamabad-points.geojson"));
			JSONObject jsonObject = (JSONObject) obj;
			jsonList = (JSONArray) jsonObject.get("features");
			//			for (int i=0; i < jsonList.size(); i++) {
			//				System.out.println(jsonList.get(i));
			//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonList.toJSONString();  
	} 

	@GetMapping("/writedatainfile")  
	public String writeDataInFile() throws IOException{
		FileWriter osduWriter = new FileWriter("/home/irfan/Documents/db/test.json");
		String sql="Select * from products";
		osduWriter.write(dataService.getRecords(3,sql).toString());
		osduWriter.close();
		System.out.println("Successfully wrote to the file.");
		System.out.println("Test.");
		return dataService.getRecords(3,sql).toString();  
	} 
	
	@GetMapping("/getdatawithquery/{query}")  
	public String getDataWithQuery(@PathVariable String query){  
		System.out.println("query===="+query);
		//String sql="Select id,name,token from products limit ? offset ?";
		String sql="Select "+query+" from products";
		
		System.out.println("query===="+query);
		return dataService.getRecords(3,sql).toString();  
	} 
	
	@GetMapping("/getdatawithquery2/{query}")  
	public String getDataWithQuery2(@PathVariable String query){  
		System.out.println("query==22=="+query);
		//String sql="Select id,name,token from products limit ? offset ?";
		String sql="Select "+query+" from login";
		
		System.out.println("query===="+query);
		return dataService.getRecordsFromPg(3,sql).toString();  
	} 
	
	@GetMapping("/getdatawithqueryh2/{query}")  
	public String getDataWithQueryh2(@PathVariable String query){  
		System.out.println("query==22=="+query);
		//String sql="Select id,name,token from products limit ? offset ?";
		String sql="Select "+query+" from tables";
		sql="select * from test ";
		
		System.out.println("query===="+query);
		return dataService.getRecordsFromH2(3,sql).toString();  
	} 
	
	@GetMapping("/getdataFrommultipledatasources/{query}/{tableName}/{dataSource}")  
	public String getDataFromMultipleDataSources(@PathVariable String query,
			@PathVariable String tableName,
			@PathVariable String dataSource){
		return dataService.getRecordsFromMultipleDb(query,tableName,dataSource).toString();  
	} 
	
	@GetMapping("/getDataWhereFromMultipleDatasources/{query}/{whereId}/{tableName}/{dataSource}")  
	public String getDataFromMultipleDataSourcesWhere(@PathVariable String query,
			@PathVariable String whereId,
			@PathVariable String tableName,
			@PathVariable String dataSource){
		return dataService.getRecordsFromMultipleDb(query,whereId,tableName,dataSource).toString();  
	} 
}
