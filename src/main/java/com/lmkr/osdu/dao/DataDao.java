package com.lmkr.osdu.dao;

import java.sql.ResultSet;

import org.json.JSONArray;
import org.json.JSONObject;

public interface DataDao {
	public long getTotalRecords(String sql);
	//public JSONObject getData(ResultSet rs);
	public JSONArray getRecords(long totalRecords,String sql);
	public JSONArray getRecordsFromPostgres(long totalRecords,String sql);
	public JSONArray getRecordsFromH2(long totalRecords,String sql);
	public JSONArray getRecordsFromMultipleDb(String sql,String dataSource);

}
