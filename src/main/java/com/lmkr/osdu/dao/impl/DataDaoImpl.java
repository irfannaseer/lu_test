package com.lmkr.osdu.dao.impl;

import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lmkr.osdu.dao.DataDao;
import com.lmkr.osdu.elastic.ElasticClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Transactional
@Repository
public class DataDaoImpl  implements DataDao {
	
	@Autowired
	 @Qualifier("jdbcTemplate1")
	 private JdbcTemplate jdbcTemplate1;
	 
	 @Autowired
	 @Qualifier("jdbcTemplate2")
	 private JdbcTemplate jdbcTemplate2;
	 
	 @Autowired
	  @Qualifier("mysqlJdbcTemplate")
	  private NamedParameterJdbcTemplate mysqlJdbcTemplate;
	 
	 @Autowired
	  @Qualifier("pgJdbcTemplate")
	  private NamedParameterJdbcTemplate pgJdbcTemplate;
	 


//	@Autowired
//	DataSource dataSource;
//
//	@PostConstruct
//	public void initialize() {
//		setDataSource(dataSource);
//		setJdbcTemplate(jdbcTemplate1);
//		//setJdbcTemplate(jdbcTemplate2);
//		
//	}

	public DataDaoImpl() {
		//configHelper = new ConfigHelper();
	}

	@Override
	public long getTotalRecords(String sql){
		long totalRecords=0;

		//String sql = System.getenv(configHelper.getKeyValue(ConfigHelper.allDataCount));
		//String sql = System.getenv(configHelper.getKeyValue(ConfigHelper.allDataCount));
		System.out.println("\n**************SQL**************"+sql);

		try {
			totalRecords = jdbcTemplate1.queryForObject(sql,Integer.class);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return totalRecords;
	}

	//@Override
	public JSONObject getData(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		ResultSetMetaData metaData = null;
		JSONObject rowObj =null;
		metaData = rs.getMetaData();
		while (rs.next()) {
			rowObj = new JSONObject();
			int count = 0;
			// rowObj.put("_id", rs.getInt("id")+"_"+rs.getInt("fkey"));
			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				count++;

				rowObj.put(metaData.getColumnName(i), rs.getObject(metaData.getColumnName(i)));

			}
			// printJsonObject(rowObj);
			System.out.println(rowObj);
		}
		return rowObj;
	}
	public static void printJsonObject(JSONObject jsonObj) {
		jsonObj.keySet().forEach(keyStr ->
		{
			Object keyvalue = jsonObj.get(keyStr);
			System.out.println("key: "+ keyStr + " value: " + keyvalue);

			//for nested objects iteration if required
			//if (keyvalue instanceof JSONObject)
			//    printJsonObject((JSONObject)keyvalue);
		});

		Map<String, Object> mappedJson = jsonObj.toMap();
		for (Map.Entry<String, Object> entry : mappedJson.entrySet()) {
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
	}

	@Override
	public JSONArray getRecords(long totalRecords,String sql){
		String startTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		JSONArray jsonArray = new JSONArray();
		JSONArray jArray =new JSONArray();
		jsonArray=  (JSONArray) jdbcTemplate1.query(sql,  new ResultSetExtractor() {

			public JSONArray extractData(
					ResultSet rs) throws SQLException, DataAccessException {
				long count=0;

				ResultSetMetaData metaData = null;
				try {
					metaData = rs.getMetaData();
					while (rs.next()) {
						JSONObject rowObj = new JSONObject();
						for (int i = 1; i <= metaData.getColumnCount(); i++) {
							rowObj.put(metaData.getColumnName(i), rs.getObject(metaData.getColumnName(i)));
						}
						jArray.put(rowObj);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return jArray;
			}
		});
		//System.out.println("rowObj2 ==========="+jArray);

		String endTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		//System.out.println("startTime========="+startTimeStamp);
		//System.out.println("endTime========="+endTimeStamp);
		return jArray;
	}
	
	@Override
	public JSONArray getRecordsFromPostgres(long totalRecords,String sql){
		String startTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		JSONArray jsonArray = new JSONArray();
		JSONArray jArray =new JSONArray();
		System.out.println("\n**************SQL**************"+sql);
		System.out.println("\n**************getDataSource**************"+jdbcTemplate2.getDataSource());
		jsonArray=  (JSONArray) jdbcTemplate2.query(sql,  new ResultSetExtractor() {

			public JSONArray extractData(
					ResultSet rs) throws SQLException, DataAccessException {
				long count=0;

				ResultSetMetaData metaData = null;
				try {
					metaData = rs.getMetaData();
					while (rs.next()) {
						JSONObject rowObj = new JSONObject();
						for (int i = 1; i <= metaData.getColumnCount(); i++) {
							rowObj.put(metaData.getColumnName(i), rs.getObject(metaData.getColumnName(i)));
						}
						jArray.put(rowObj);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return jArray;
			}
		});
		System.out.println("rowObj2 ==========="+jArray);

		String endTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		System.out.println("startTime========="+startTimeStamp);
		System.out.println("endTime========="+endTimeStamp);
		return jArray;
	}

	@Override
	public JSONArray getRecordsFromH2(long totalRecords, String sql) {
		// TODO Auto-generated method stub
		String startTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		JSONArray jsonArray = new JSONArray();
		JSONArray jArray =new JSONArray();
		System.out.println("\n**************SQL**************"+sql);
		System.out.println("\n**************getDataSource**************"+jdbcTemplate2.getDataSource());
		jsonArray=  (JSONArray) mysqlJdbcTemplate.query(sql,  new ResultSetExtractor() {

			public JSONArray extractData(
					ResultSet rs) throws SQLException, DataAccessException {
				long count=0;

				ResultSetMetaData metaData = null;
				try {
					metaData = rs.getMetaData();
					while (rs.next()) {
						JSONObject rowObj = new JSONObject();
						for (int i = 1; i <= metaData.getColumnCount(); i++) {
							rowObj.put(metaData.getColumnName(i), rs.getObject(metaData.getColumnName(i)));
						}
						jArray.put(rowObj);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return jArray;
			}
		});
		System.out.println("rowObj2 ==========="+jArray);

		String endTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		System.out.println("startTime========="+startTimeStamp);
		System.out.println("endTime========="+endTimeStamp);
		return jArray;
	}
	
	@Override
	public JSONArray getRecordsFromMultipleDb(String sql, String dataSource) {
		// TODO Auto-generated method stub
		String startTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		JSONArray jsonArray = new JSONArray();
		JSONArray jArray =new JSONArray();
		NamedParameterJdbcTemplate jdbcTemplate=null;
		if(dataSource.equals("pg"))
			jdbcTemplate=pgJdbcTemplate;
		if(dataSource.equals("mysql"))
			jdbcTemplate=mysqlJdbcTemplate;
			
		System.out.println("\n**************SQL**************"+sql);
		System.out.println("\n**************getDataSource**************"+jdbcTemplate2.getDataSource());
		jsonArray=  (JSONArray) jdbcTemplate1.query(sql,  new ResultSetExtractor() {
			ElasticClient elasticClient=new ElasticClient();
			public JSONArray extractData(
					ResultSet rs) throws SQLException, DataAccessException {
				long count=0;

				//ResultSetMetaData metaData = null;
				try {
					elasticClient.elasticIndexOsdu(rs);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return jArray;
			}
		});
		System.out.println("rowObj2 ==========="+jArray);

		String endTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		System.out.println("startTime========="+startTimeStamp);
		System.out.println("endTime========="+endTimeStamp);
		return jArray;
	}
/*
	@Override
	public JSONArray getRecordsFromMultipleDb(String sql, String dataSource) {
		// TODO Auto-generated method stub
		String startTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		JSONArray jsonArray = new JSONArray();
		JSONArray jArray =new JSONArray();
		NamedParameterJdbcTemplate jdbcTemplate=null;
		if(dataSource.equals("pg"))
			jdbcTemplate=jdbcTemplate=pgJdbcTemplate;;
		if(dataSource.equals("mysql"))
			jdbcTemplate=mysqlJdbcTemplate;
			
		System.out.println("\n**************SQL**************"+sql);
		System.out.println("\n**************getDataSource**************"+jdbcTemplate2.getDataSource());
		jsonArray=  (JSONArray) jdbcTemplate.query(sql,  new ResultSetExtractor() {

			public JSONArray extractData(
					ResultSet rs) throws SQLException, DataAccessException {
				long count=0;

				ResultSetMetaData metaData = null;
				try {
					metaData = rs.getMetaData();
					while (rs.next()) {
						JSONObject rowObj = new JSONObject();
						for (int i = 1; i <= metaData.getColumnCount(); i++) {
							rowObj.put(metaData.getColumnName(i), rs.getObject(metaData.getColumnName(i)));
						}
						jArray.put(rowObj);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return jArray;
			}
		});
		System.out.println("rowObj2 ==========="+jArray);

		String endTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
		System.out.println("startTime========="+startTimeStamp);
		System.out.println("endTime========="+endTimeStamp);
		return jArray;
	}
*/
	//	@Override
	//	 public JSONObject getRecords(long totalRecords,String sql){
	//	        long totalRecordsInserted=0;
	//	        boolean flag=true;
	//	        totalRecords=3;
	//	        long limit=1;
	//	        long offset = 0;
	//	        String startTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
	//	        JSONObject rowObj2 =null;
	//	        JSONObject rowObj3 =null;
	//	        //String sql = System.getenv(configHelper.getKeyValue(ConfigHelper.allDataInsert));
	//
	//	        while(flag) {
	//	        	rowObj2=  (JSONObject) getJdbcTemplate().query(sql, new Long[]{limit, offset}, new ResultSetExtractor() {
	//
	//	                public Object extractData(
	//	                        ResultSet rs) throws SQLException, DataAccessException {
	//	                        long count=0;
	//	                        JSONObject rowObj =null;
	//	                    try {
	//	                    	rowObj=getData(rs);//differiantial
	//	                        //System.out.println("count====="+count);
	//	                    } catch (Exception e) {
	//	                        e.printStackTrace();
	//	                    }
	//	                   // rowObj2=rowObj;
	//	                    return rowObj;
	//	                }
	//	            });
	//	            offset = offset + limit;
	//	            System.out.println("offset ==========="+offset+"\nlimit========"+limit);
	//	            if(offset>=totalRecords){
	//	                flag=false;
	//	            }
	//	        }
	//	        System.out.println("Total Records Selected ==========="+totalRecords);
	//	        System.out.println("Total Records Inserted ==========="+totalRecordsInserted);
	//	        String endTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss z").format(new Date());
	//	        System.out.println("startTime========="+startTimeStamp);
	//	        System.out.println("endTime========="+endTimeStamp);
	//	        return rowObj2;
	//	    }

}
