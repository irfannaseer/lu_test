package com.lmkr.osdu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OsduApplication {

	public static void main(String[] args) {
		SpringApplication.run(OsduApplication.class, args);
	}

}
