FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/osdu-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/app.jar"]
VOLUME /tmp
COPY ${JAR_FILE} app.jar